package com.organization.template.unit.service.auth;

import com.organization.template.exception.user.RoleNotFoundException;
import com.organization.template.exception.user.UserAlreadyExistsException;
import com.organization.template.infrastructure.BaseUnitTest;
import com.organization.template.model.auth.UserPrincipal;
import com.organization.template.repository.RoleRepository;
import com.organization.template.repository.UserRepository;
import com.organization.template.repository.model.user.Role;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.auth.impl.RegisterUserServiceImpl;
import com.organization.template.service.email.UserActivationEmailService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.mail.MessagingException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class RegisterUserServiceTest extends BaseUnitTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private RoleRepository roleRepository;

  @Mock
  private PasswordEncoder passwordEncoder;

  @Mock
  private UserActivationEmailService userActivationEmailService;

  @InjectMocks
  private RegisterUserServiceImpl registerUserService;

  @Test
  @DisplayName("Successfully registered user")
  void registerUserTest() {
    User user = helperService.getUser();
    Role userRole = helperService.getUserRole();
    Optional<Role> optionalRole = Optional.of(userRole);

    doReturn(Optional.empty())
            .when(userRepository).findByEmail(any());
    doReturn(optionalRole)
            .when(roleRepository).findBySlug(any());
    when(passwordEncoder.encode(any()))
            .thenReturn("password");

    registerUserService.registerUser(user);

    verify(userRepository).findByEmail(any());
  }

  @Test
  @DisplayName("User already exists")
  void userAlreadyExistsTest() {
    User user = helperService.getUser();
    UserPrincipal userPrincipal = new UserPrincipal(user);

    doReturn(Optional.of(userPrincipal))
            .when(userRepository).findByEmail(any());

    assertThatThrownBy(() -> registerUserService.registerUser(user))
            .isInstanceOf(UserAlreadyExistsException.class);
  }

  @Test
  @DisplayName("Role not found")
  void roleNotFoundTest() {
    User user = helperService.getUser();

    doReturn(Optional.empty())
            .when(userRepository).findByEmail(any());
    doReturn(Optional.empty())
            .when(roleRepository).findBySlug(any());

    assertThatThrownBy(() -> registerUserService.registerUser(user))
            .isInstanceOf(RoleNotFoundException.class);
  }

  @Test
  @DisplayName("Email sending error")
  void emailErrorTest() throws MessagingException {
    User user = helperService.getUser();
    Role userRole = helperService.getUserRole();

    doReturn(Optional.empty())
            .when(userRepository).findByEmail(any());
    doReturn(Optional.of(userRole))
            .when(roleRepository).findBySlug(any());
    doThrow(MessagingException.class)
            .when(userActivationEmailService).sendUserActivationEmail(any(), any());

    registerUserService.registerUser(user);

    verify(userActivationEmailService).sendUserActivationEmail(any(), any());
  }

}
