package com.organization.template.service.auth;

import com.organization.template.dto.auth.TokensDTO;

import javax.servlet.http.HttpServletRequest;

/**
 * Refresh user token service
 */
public interface RefreshTokenService {

  /**
   * Refresh user token
   * @param httpServletRequest        Request
   * @return                          Access token
   */
  TokensDTO refreshToken(HttpServletRequest httpServletRequest);

}
