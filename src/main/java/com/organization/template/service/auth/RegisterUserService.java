package com.organization.template.service.auth;

import com.organization.template.repository.model.user.User;

/**
 * Register user service
 */
public interface RegisterUserService {

  /**
   * Register user
   * @param user       User information
   */
  void registerUser(User user);

}
