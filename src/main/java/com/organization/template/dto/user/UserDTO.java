package com.organization.template.dto.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.organization.template.constants.MiscConstants;
import com.organization.template.model.enums.UserRole;
import com.organization.template.repository.model.user.Role;
import com.organization.template.repository.model.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * User DTO
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO {

  @JsonProperty
  private UUID id;

  @JsonProperty
  private String email;

  @JsonProperty
  private String username;

  @JsonProperty("first_name")
  private String firstName;

  @JsonProperty("last_name")
  private String lastName;

  @JsonProperty
  private String role;

  @JsonProperty
  private Boolean active;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = MiscConstants.DATE_TIME_FORMAT)
  @JsonProperty("created_at")
  private Timestamp createdAt;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = MiscConstants.DATE_TIME_FORMAT)
  @JsonProperty("last_login")
  private Timestamp lastLogin;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = MiscConstants.DATE_TIME_FORMAT)
  @JsonProperty("deactivated_at")
  private Timestamp deactivatedAt;

  public UserDTO(User user) {
    this.id = user.getId();
    this.email = user.getEmail();
    this.username = user.getUsername();
    this.firstName = user.getFirstName();
    this.lastName = user.getLastName();
    this.role = getDefaultRole(user);
    this.active = user.getActive();
    this.createdAt = user.getCreatedAt();
    this.lastLogin = user.getLastLogin();
    this.deactivatedAt = user.getDeactivatedAt();
  }

  private String getDefaultRole(User user) {
    Set<UserRole> userRoles = user.getRoles().stream()
            .map(Role::getSlug)
            .collect(Collectors.toSet());

    if (userRoles.contains(UserRole.ROLE_SUPER_ADMIN)) {
      return "Super administrator";
    }
    if (userRoles.contains(UserRole.ROLE_ADMIN)) {
      return "Administrator";
    }
    if (userRoles.contains(UserRole.ROLE_USER)) {
      return "User";
    }
    return "None";
  }

}
