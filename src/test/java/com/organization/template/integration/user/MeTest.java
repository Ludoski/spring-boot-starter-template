package com.organization.template.integration.user;

import com.organization.template.constants.ApiConstants;
import com.organization.template.dto.auth.TokensDTO;
import com.organization.template.infrastructure.BaseIntegrationTest;
import com.organization.template.repository.model.user.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class MeTest extends BaseIntegrationTest {

  @Test
  @DisplayName("200 - Get user")
  void successTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    helperService.saveUser(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    get(ApiConstants.ME)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.message").isNotEmpty());
  }

  @Test
  @DisplayName("401 - Unauthorized")
  void unauthorizedTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    helperService.saveUser(email, password, true);

    // Check response
    mockMvc.perform(
                    get(ApiConstants.ME)
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isUnauthorized());
  }

  @Test
  @DisplayName("404 - Not found")
  void notFoundTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Delete user
    helperService.deleteUser(user);

    // Check response
    mockMvc.perform(
                    get(ApiConstants.ME)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isNotFound());
  }

  @Test
  @DisplayName("405 - Wrong HTTP method")
  void wrongHttpMethodTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    helperService.saveUser(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    put(ApiConstants.ME)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isMethodNotAllowed());
  }

}
