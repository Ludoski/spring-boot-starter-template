package com.organization.template.dto.auth;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.organization.template.constants.MiscConstants;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

/**
 * Reset password new password DTO
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PasswordResetFinishDTO {

  @NotNull
  @JsonProperty
  private UUID token;

  @Size(min = MiscConstants.PASSWORD_MIN_LENGTH)
  @NotBlank
  @NotNull
  @JsonProperty
  private String password;

}
