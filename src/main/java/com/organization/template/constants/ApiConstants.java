package com.organization.template.constants;

public class ApiConstants {

  private ApiConstants() {
    throw new IllegalStateException("Utility class");
  }

  // Auth
  public static final String BASIC_AUTH_PATH = "/auth";
  public static final String REGISTER = BASIC_AUTH_PATH + "/register";
  public static final String LOGIN = BASIC_AUTH_PATH + "/login";
  public static final String REFRESH_TOKEN = BASIC_AUTH_PATH + "/refresh";
  public static final String CHANGE_PASSWORD = BASIC_AUTH_PATH + "/password";
  public static final String CONFIRM_EMAIL = BASIC_AUTH_PATH + "/confirm-email/{activation-token}";
  public static final String PASSWORD_RESET = BASIC_AUTH_PATH + "/password-reset";

  // v1
  public static final String BASIC_V1_PATH = "/v1";

  // Users v1
  public static final String BASIC_USERS_PATH = BASIC_V1_PATH + "/users";
  public static final String ME = BASIC_USERS_PATH + "/me";
  public static final String GET_USER_BY_ID = BASIC_USERS_PATH + "/{id}";
  public static final String UPDATE_USER = BASIC_USERS_PATH + "/{id}";
  public static final String DELETE_USER = BASIC_USERS_PATH + "/{id}";
  public static final String TOGGLE_USER_ACTIVE = BASIC_USERS_PATH + "/{id}/toggle-active";
  public static final String FILTER_ALL_USERS = BASIC_USERS_PATH + "/filter";

  // Util
  public static final String BASIC_UTIL_PATH = "/util";
  public static final String STATUS = BASIC_UTIL_PATH + "/status";

}
