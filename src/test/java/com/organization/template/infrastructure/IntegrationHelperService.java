package com.organization.template.infrastructure;

import com.organization.template.dto.auth.LoginDTO;
import com.organization.template.exception.user.RoleNotFoundException;
import com.organization.template.model.enums.UserRole;
import com.organization.template.repository.RoleRepository;
import com.organization.template.repository.UserRepository;
import com.organization.template.repository.model.user.Role;
import com.organization.template.repository.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * Helper service for integration testing
 */
@Service
@Transactional
public class IntegrationHelperService {

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private PasswordEncoder passwordEncoder;

  public String getRandomEmail() {
    return UUID.randomUUID() + "@template.com";
  }

  public String getRandomString() {
    return UUID.randomUUID().toString();
  }

  public LoginDTO getLoginDTO() {
    return LoginDTO.builder()
            .email(getRandomEmail())
            .password(getRandomString())
            .build();
  }

  public User saveUser(String email, String password, boolean active) {
    User user = User.builder()
            .id(UUID.randomUUID())
            .email(email)
            .password(passwordEncoder.encode(password))
            .username("user")
            .firstName(getRandomString())
            .lastName(getRandomString())
            .roles(Set.of(getUserRole()))
            .active(active)
            .createdAt(Timestamp.valueOf(Instant.now().atOffset(ZoneOffset.UTC).toLocalDateTime()))
            .activationToken(UUID.randomUUID())
            .passwordResetToken(UUID.randomUUID())
            .build();

    return userRepository.save(user);
  }

  public User saveAdmin(String email, String password, boolean active) {
    User admin = User.builder()
            .id(UUID.randomUUID())
            .email(email)
            .password(passwordEncoder.encode(password))
            .username("admin")
            .firstName(getRandomString())
            .lastName(getRandomString())
            .roles(Set.of(getUserRole(), getAdminRole()))
            .active(active)
            .createdAt(Timestamp.valueOf(Instant.now().atOffset(ZoneOffset.UTC).toLocalDateTime()))
            .activationToken(UUID.randomUUID())
            .passwordResetToken(UUID.randomUUID())
            .build();

    return userRepository.save(admin);
  }

  public User saveSuperAdmin(String email, String password, boolean active) {
    User admin = User.builder()
            .id(UUID.randomUUID())
            .email(email)
            .password(passwordEncoder.encode(password))
            .username("admin")
            .firstName(getRandomString())
            .lastName(getRandomString())
            .roles(Set.of(getUserRole(), getAdminRole(), getSuperAdminRole()))
            .active(active)
            .createdAt(Timestamp.valueOf(Instant.now().atOffset(ZoneOffset.UTC).toLocalDateTime()))
            .activationToken(UUID.randomUUID())
            .passwordResetToken(UUID.randomUUID())
            .build();

    return userRepository.save(admin);
  }

  public void deleteUser(User user) {
    userRepository.delete(user);
  }

  public Role getUserRole() {
    Optional<Role> optionalRole = roleRepository.findBySlug(UserRole.ROLE_USER);
    if (optionalRole.isEmpty()) {
      throw new RoleNotFoundException();
    }
    return optionalRole.get();
  }

  public Role getAdminRole() {
    Optional<Role> optionalRole = roleRepository.findBySlug(UserRole.ROLE_ADMIN);
    if (optionalRole.isEmpty()) {
      throw new RoleNotFoundException();
    }
    return optionalRole.get();
  }

  public Role getSuperAdminRole() {
    Optional<Role> optionalRole = roleRepository.findBySlug(UserRole.ROLE_SUPER_ADMIN);
    if (optionalRole.isEmpty()) {
      throw new RoleNotFoundException();
    }
    return optionalRole.get();
  }

}
