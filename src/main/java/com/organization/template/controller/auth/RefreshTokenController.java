package com.organization.template.controller.auth;

import com.organization.template.config.OpenApiConfiguration;
import com.organization.template.constants.ApiConstants;
import com.organization.template.constants.RoleConstants;
import com.organization.template.dto.auth.TokensDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Refresh user token controller
 */
public interface RefreshTokenController {

  /**
   * Refresh user token
   * @param httpServletRequest        Request
   * @return                          Access token
   */
  @Tag(name = OpenApiConfiguration.AUTH_TAG)
  @Operation(summary = "Refresh user token")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "New access token generated"),
          @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
          @ApiResponse(responseCode = "404", description = "User not found", content = @Content),
          @ApiResponse(responseCode = "405", description = "HTTP method must be GET", content = @Content)
  })
  @PreAuthorize(RoleConstants.HAS_ROLE_USER)
  @GetMapping(path = ApiConstants.REFRESH_TOKEN, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  TokensDTO refreshToken(HttpServletRequest httpServletRequest);

}
