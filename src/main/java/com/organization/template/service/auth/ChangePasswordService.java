package com.organization.template.service.auth;

import com.organization.template.dto.auth.ChangePasswordDTO;

/**
 * Change user password
 */
public interface ChangePasswordService {

  /**
   * Change user password
   * @param changePasswordDTO              User passwords
   */
  void changePassword(ChangePasswordDTO changePasswordDTO);

}
