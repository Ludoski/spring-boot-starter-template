package com.organization.template.controller.user;

import com.organization.template.config.OpenApiConfiguration;
import com.organization.template.constants.ApiConstants;
import com.organization.template.constants.RoleConstants;
import com.organization.template.dto.user.UserDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

/**
 * Toggle user active/inactive by id controller
 */
public interface ToggleUserActiveController {

  /**
   * Toggle user active by id
   * @param id                    User id
   * @return                      Updated user information
   */
  @Tag(name = OpenApiConfiguration.USER_TAG)
  @Operation(summary = "Toggle user active")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "User updated"),
          @ApiResponse(responseCode = "400", description = "User id must be UUID", content = @Content),
          @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
          @ApiResponse(responseCode = "403", description = "Not admin or trying to toggle super admin", content = @Content),
          @ApiResponse(responseCode = "404", description = "User not found", content = @Content),
          @ApiResponse(responseCode = "405", description = "HTTP method must be PATCH", content = @Content)
  })
  @PreAuthorize(RoleConstants.HAS_ROLE_ADMIN)
  @PatchMapping(path = ApiConstants.TOGGLE_USER_ACTIVE, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  UserDTO toggleUserActive(@PathVariable UUID id);

}
