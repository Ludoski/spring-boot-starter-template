package com.organization.template.controller.auth.impl;

import com.organization.template.controller.auth.ChangePasswordController;
import com.organization.template.dto.auth.ChangePasswordDTO;
import com.organization.template.service.auth.ChangePasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @see ChangePasswordController
 */
@RestController
public class ChangePasswordControllerImpl implements ChangePasswordController {

  @Autowired
  private ChangePasswordService changePasswordService;

  /**
   * @see ChangePasswordController#changePassword(ChangePasswordDTO)
   */
  @Override
  public void changePassword(ChangePasswordDTO changePasswordDTO) {
    changePasswordService.changePassword(changePasswordDTO);
  }

}
