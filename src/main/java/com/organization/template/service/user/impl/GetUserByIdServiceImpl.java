package com.organization.template.service.user.impl;

import com.organization.template.exception.auth.ForbiddenException;
import com.organization.template.exception.user.UserNotFoundException;
import com.organization.template.model.enums.UserRole;
import com.organization.template.repository.UserRepository;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.auth.CurrentUserService;
import com.organization.template.service.user.GetUserByIdService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

/**
 * @see GetUserByIdService
 */
@Slf4j
@Service
public class GetUserByIdServiceImpl implements GetUserByIdService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CurrentUserService currentUserService;

  /**
   * @see GetUserByIdService#getUserById(UUID)
   */
  @Transactional
  @Override
  public User getUserById(UUID id) {
    // Get current user id
    UUID currentUserId = currentUserService.getId();

    log.info("User ({}) is trying to fetch user with id ({}).", currentUserId, id);

    // Return if user is not admin and want to get other
    if (!currentUserService.hasAuthority(UserRole.ROLE_ADMIN) &&
            !currentUserId.equals(id)) {
      log.error("User ({}) is forbidden to fetch user ({})", currentUserId, id);
      throw new ForbiddenException();
    }

    // Get user if exists
    Optional<User> optionalUser = userRepository.findById(id);
    if (optionalUser.isEmpty()) {
      log.error("User with id ({}) not found.", id);
      throw new UserNotFoundException();
    }

    log.info("User ({}) successfully fetched user with id ({}).", currentUserId, id);

    return optionalUser.get();
  }

}
