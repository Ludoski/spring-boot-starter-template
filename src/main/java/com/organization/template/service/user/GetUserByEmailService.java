package com.organization.template.service.user;

import com.organization.template.repository.model.user.User;

/**
 * Getting user information by email service
 */
public interface GetUserByEmailService {

  /**
   * Get user by id
   * @param email         User email
   * @return              User model
   */
  User getUserByEmail(String email);

}
