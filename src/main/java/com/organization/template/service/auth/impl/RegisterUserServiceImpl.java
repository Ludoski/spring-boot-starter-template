package com.organization.template.service.auth.impl;

import com.organization.template.exception.user.RoleNotFoundException;
import com.organization.template.exception.user.UserAlreadyExistsException;
import com.organization.template.model.enums.UserRole;
import com.organization.template.repository.RoleRepository;
import com.organization.template.repository.UserRepository;
import com.organization.template.repository.model.user.Role;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.auth.RegisterUserService;
import com.organization.template.service.email.UserActivationEmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * @see RegisterUserService
 */
@Slf4j
@Service
public class RegisterUserServiceImpl implements RegisterUserService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private UserActivationEmailService userActivationEmailService;

  /**
   * @see RegisterUserService#registerUser(User)
   */
  @Transactional
  @Override
  public void registerUser(User user) {
    log.info("Trying to create user with email ({})", user.getEmail());

    // Check if user already exists
    Optional<User> optionalUser = userRepository.findByEmail(user.getEmail());
    if (optionalUser.isPresent()) {
      log.error("User with email ({}) already exists. User creation aborted.", user.getEmail());
      throw new UserAlreadyExistsException();
    }

    // Encode user password
    user.setPassword(passwordEncoder.encode(user.getPassword()));

    // Set default user role
    UserRole userRole = UserRole.ROLE_USER;
    Optional<Role> optionalRole = roleRepository.findBySlug(userRole);
    if (optionalRole.isEmpty()) {
      log.error("Role with slug ({}) do not exists. User creation aborted.", userRole);
      throw new RoleNotFoundException();
    }
    Set<Role> roles = new HashSet<>();
    roles.add(optionalRole.get());
    user.setRoles(roles);

    // Set user inactive until email is confirmed
    user.setActive(false);

    // Get current time in UTC
    Timestamp now = Timestamp.valueOf(Instant.now().atOffset(ZoneOffset.UTC).toLocalDateTime());

    // Set user deactivated time
    user.setDeactivatedAt(now);

    // Set user creation time
    user.setCreatedAt(now);

    // Create activation token
    user.setActivationToken(UUID.randomUUID());

    // Save user
    userRepository.save(user);

    // Send user activation email
    try {
      log.info("Sending activation email to ({}).", user.getEmail());
      userActivationEmailService.sendUserActivationEmail(user.getEmail(), user.getActivationToken());
    } catch (MessagingException e) {
      log.error("Unable to send user activation email for user ({}).", user.getId(), e);
    }

    log.info("Successfully created user ({}).", user.getId());
  }

}
