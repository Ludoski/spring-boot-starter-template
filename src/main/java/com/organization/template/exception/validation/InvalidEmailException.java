package com.organization.template.exception.validation;

import com.organization.template.exception.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * Invalid user email exception
 */
public class InvalidEmailException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "Invalid email.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.BAD_REQUEST;

  public InvalidEmailException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
