package com.organization.template.exception.validation;

import com.organization.template.exception.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * Invalid user password exception
 */
public class InvalidPasswordException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "Invalid password.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.BAD_REQUEST;

  public InvalidPasswordException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
