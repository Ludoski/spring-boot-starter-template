package com.organization.template.service.user;

import com.organization.template.repository.model.user.User;

import java.util.UUID;

/**
 * Getting user information by id service
 */
public interface GetUserByIdService {

  /**
   * Get user by id
   * @param id        User id
   * @return          User model
   */
  User getUserById(UUID id);

}
