package com.organization.template.service.auth;

import org.springframework.security.core.userdetails.UserDetails;

import java.util.Map;

/**
 * Service for jwt token manipulation
 */
public interface JwtTokenService {

  /**
   * Create access token
   * @param userDetails             User details
   * @return                        Access token
   */
  String createJwtAccessToken(UserDetails userDetails);

  /**
   * Create refresh token
   * @param userDetails            User details
   * @return                       Refresh token
   */
  String createJwtRefreshToken(UserDetails userDetails);

  /**
   * Decode token
   * @param token     Jwt token
   * @return          Map of claims
   */
  Map<String, String> decodeToken(String token);

}
