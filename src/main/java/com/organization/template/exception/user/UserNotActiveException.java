package com.organization.template.exception.user;

import com.organization.template.exception.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * User not found exception
 */
public class UserNotActiveException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "User is not active.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.NOT_FOUND;

  public UserNotActiveException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
