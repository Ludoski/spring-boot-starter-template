package com.organization.template.service.user;

import com.organization.template.repository.model.user.User;

/**
 * Getting personal user information about service
 */
public interface MeService {

  /**
   * Get personal information
   * @return          User model
   */
  User me();

}
