package com.organization.template.unit.controller.auth;

import com.organization.template.controller.auth.impl.ChangePasswordControllerImpl;
import com.organization.template.dto.auth.ChangePasswordDTO;
import com.organization.template.infrastructure.BaseUnitTest;
import com.organization.template.service.auth.ChangePasswordService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class ChangePasswordControllerTest extends BaseUnitTest {

  @Mock
  private ChangePasswordService changePasswordService;

  @InjectMocks
  private ChangePasswordControllerImpl changePasswordController;

  @Test
  @DisplayName("Successfully changed password")
  void changePasswordTest() {
    ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
            .oldPassword(helperService.getRandomString())
            .newPassword(helperService.getRandomString())
            .build();

    changePasswordController.changePassword(changePasswordDTO);

    verify(changePasswordService, times(1)).changePassword(changePasswordDTO);
  }

}
