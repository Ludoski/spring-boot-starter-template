package com.organization.template.service.auth;

import com.organization.template.dto.auth.TokensDTO;

/**
 * User login service
 */
public interface LoginService {

  /**
   * User login
   * @param email           Email
   * @param password        Password
   * @return                Access and refresh tokens
   */
  TokensDTO login(String email, String password);

}
