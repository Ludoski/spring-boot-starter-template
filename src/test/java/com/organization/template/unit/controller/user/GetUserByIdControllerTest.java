package com.organization.template.unit.controller.user;

import com.organization.template.controller.user.impl.GetUserByIdControllerImpl;
import com.organization.template.dto.user.UserDTO;
import com.organization.template.infrastructure.BaseUnitTest;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.user.GetUserByIdService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class GetUserByIdControllerTest extends BaseUnitTest {

  @Mock
  private GetUserByIdService getUserByIdService;

  @InjectMocks
  private GetUserByIdControllerImpl getUserByIdController;

  @Test
  @DisplayName("Successfully get user")
  void getUserByIdTest() {
    User user = helperService.getUser();

    when(getUserByIdService.getUserById(any()))
            .thenReturn(user);

    UserDTO result = getUserByIdController.getUserById(any());

    assertThat(result.getEmail()).isEqualTo(user.getEmail());
  }

}
