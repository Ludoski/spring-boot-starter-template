package com.organization.template.exception.user;

import com.organization.template.exception.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * User not found exception
 */
public class RoleNotFoundException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "Role not found.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.NOT_FOUND;

  public RoleNotFoundException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
