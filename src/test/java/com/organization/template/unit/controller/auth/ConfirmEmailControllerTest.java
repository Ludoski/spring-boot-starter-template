package com.organization.template.unit.controller.auth;

import com.organization.template.controller.auth.impl.ConfirmEmailControllerImpl;
import com.organization.template.infrastructure.BaseUnitTest;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.auth.ConfirmEmailService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

class ConfirmEmailControllerTest extends BaseUnitTest {

  @Mock
  private ConfirmEmailService confirmEmailService;

  @InjectMocks
  private ConfirmEmailControllerImpl confirmEmailController;

  @Test
  @DisplayName("Successful confirmation")
  void successTest() {
    User user = helperService.getUser();

    confirmEmailController.confirmEmail(user.getActivationToken());

    verify(confirmEmailService).confirmEmail(any());
  }

}
