package com.organization.template.service.email;

import javax.mail.MessagingException;
import java.util.UUID;

/**
 * Password reset email service
 */
public interface PasswordResetEmailService {

  /**
   * Send password reset email
   * @param email                 User email address
   * @param passwordResetToken    Password reset token
   */
  void sendPasswordResetEmail(String email, UUID passwordResetToken) throws MessagingException;

}
