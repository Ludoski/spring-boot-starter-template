package com.organization.template.model.enums;

/**
 * User roles enum
 */
public enum UserRole {
  ROLE_SUPER_ADMIN,
  ROLE_ADMIN,
  ROLE_USER
}
