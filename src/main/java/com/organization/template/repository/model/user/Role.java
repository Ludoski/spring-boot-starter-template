package com.organization.template.repository.model.user;

import com.organization.template.model.enums.UserRole;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

/**
 * Role database model
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "roles")
public class Role implements Serializable {

  @Id
  @GeneratedValue
  @Column(name = "id")
  private UUID id;

  @Enumerated(EnumType.STRING)
  @Column(name = "slug")
  private UserRole slug;

  @Column(name = "name")
  private String name;

  @Column(name = "description")
  private String description;

}
