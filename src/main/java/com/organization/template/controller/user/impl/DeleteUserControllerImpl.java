package com.organization.template.controller.user.impl;

import com.organization.template.controller.user.DeleteUserController;
import com.organization.template.service.user.DeleteUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @see DeleteUserController
 */
@RestController
public class DeleteUserControllerImpl implements DeleteUserController {

  @Autowired
  private DeleteUserService deleteUserService;

  /**
   * @see DeleteUserController#deleteUser(UUID)
   */
  @Override
  public void deleteUser(UUID id) {
    deleteUserService.deleteUser(id);
  }

}
