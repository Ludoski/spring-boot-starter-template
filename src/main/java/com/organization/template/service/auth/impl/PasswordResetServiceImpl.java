package com.organization.template.service.auth.impl;

import com.organization.template.dto.auth.PasswordResetFinishDTO;
import com.organization.template.dto.auth.PasswordResetInitiateDTO;
import com.organization.template.exception.user.UserNotFoundException;
import com.organization.template.repository.UserRepository;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.auth.PasswordResetService;
import com.organization.template.service.email.PasswordResetEmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.util.Optional;
import java.util.UUID;

/**
 * @see PasswordResetService
 */
@Slf4j
@Service
public class PasswordResetServiceImpl implements PasswordResetService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private PasswordResetEmailService passwordResetEmailService;

  @Autowired
  private PasswordEncoder passwordEncoder;

  /**
   * @see PasswordResetService#passwordResetInitiate(PasswordResetInitiateDTO)
   */
  @Transactional
  @Override
  public void passwordResetInitiate(PasswordResetInitiateDTO passwordResetInitiateDTO) {
    String email = passwordResetInitiateDTO.getEmail();

    log.info("Trying to initiate password reset for email ({})).", email);

    // Get user if exists
    Optional<User> optionalUser = userRepository.findByEmail(email);
    if (optionalUser.isEmpty()) {
      log.error("User with email ({}) not found.", email);
      throw new UserNotFoundException();
    }

    User user = optionalUser.get();

    // Generate reset token
    UUID passwordResetToken = UUID.randomUUID();

    // Set password reset token
    user.setPasswordResetToken(passwordResetToken);

    // Send password reset email
    try {
      log.info("Sending password reset email to ({}).", user.getEmail());
      passwordResetEmailService.sendPasswordResetEmail(email, passwordResetToken);
    } catch (MessagingException e) {
      log.error("Unable to send password reset email for user ({}).", user.getId(), e);
    }

    log.info("Successfully initiated password reset for user ({}).", user.getId());
  }

  /**
   * @see PasswordResetService#passwordResetFinish(PasswordResetFinishDTO)
   */
  @Transactional
  @Override
  public void passwordResetFinish(PasswordResetFinishDTO passwordResetFinishDTO) {
    UUID token = passwordResetFinishDTO.getToken();

    log.info("Trying to finish password reset with password reset token ({})).", token);

    // Get user if exists
    Optional<User> optionalUser = userRepository.findByPasswordResetToken(token);
    if (optionalUser.isEmpty()) {
      log.error("User with password reset token ({}) not found.", token);
      throw new UserNotFoundException();
    }

    User user = optionalUser.get();

    // Encode user password
    String password = passwordResetFinishDTO.getPassword();
    user.setPassword(passwordEncoder.encode(password));

    // Reset password reset token
    user.setPasswordResetToken(null);

    log.info("Successfully finished password reset for user ({}).", user.getId());
  }

}
