package com.organization.template.integration.auth;

import com.organization.template.constants.ApiConstants;
import com.organization.template.dto.auth.LoginDTO;
import com.organization.template.infrastructure.BaseIntegrationTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class LoginTest extends BaseIntegrationTest {

  @Test
  @DisplayName("200 - Login successful")
  void successTest() throws Exception {
    LoginDTO loginDTO = helperService.getLoginDTO();

    // Save valid user
    helperService.saveUser(loginDTO.getEmail(), loginDTO.getPassword(), true);

    // Login and check response
    mockMvc.perform(
                    post(ApiConstants.LOGIN)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(loginDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.message.access_token").isNotEmpty())
            .andExpect(jsonPath("$.message.refresh_token").isNotEmpty());
  }

  @Test
  @DisplayName("400 - Request body is empty")
  void requestBodyIsEmptyTest() throws Exception {
    // Try to log in
    mockMvc.perform(
                    post(ApiConstants.LOGIN)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(null))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - No email in request")
  void noEmailInRequestTest() throws Exception {
    LoginDTO loginDTO = helperService.getLoginDTO();

    // Save valid user
    helperService.saveUser(loginDTO.getEmail(), loginDTO.getPassword(), true);

    // Remove email
    loginDTO.setEmail(null);

    // Try to log in
    mockMvc.perform(
                    post(ApiConstants.LOGIN)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(loginDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Wrong email format")
  void wrongEmailFormatTest() throws Exception {
    LoginDTO loginDTO = helperService.getLoginDTO();

    // Save valid user
    helperService.saveUser(loginDTO.getEmail(), loginDTO.getPassword(), true);

    // Set invalid email format
    loginDTO.setEmail("email");

    // Try to log in
    mockMvc.perform(
                    post(ApiConstants.LOGIN)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(loginDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Email empty")
  void emailEmptyTest() throws Exception {
    LoginDTO loginDTO = helperService.getLoginDTO();

    // Save valid user
    helperService.saveUser(loginDTO.getEmail(), loginDTO.getPassword(), true);

    // Set email empty
    loginDTO.setEmail("");

    // Try to log in
    mockMvc.perform(
                    post(ApiConstants.LOGIN)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(loginDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Email space")
  void emailSpaceTest() throws Exception {
    LoginDTO loginDTO = helperService.getLoginDTO();

    // Save valid user
    helperService.saveUser(loginDTO.getEmail(), loginDTO.getPassword(), true);

    // Set email space
    loginDTO.setEmail(" ");

    // Try to log in
    mockMvc.perform(
                    post(ApiConstants.LOGIN)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(loginDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - No password in request")
  void noPasswordInRequestTest() throws Exception {
    LoginDTO loginDTO = helperService.getLoginDTO();

    // Save valid user
    helperService.saveUser(loginDTO.getEmail(), loginDTO.getPassword(), true);

    // Remove password
    loginDTO.setPassword(null);

    // Try to log in
    mockMvc.perform(
                    post(ApiConstants.LOGIN)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(loginDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Password empty")
  void passwordEmptyTest() throws Exception {
    LoginDTO loginDTO = helperService.getLoginDTO();

    // Save valid user
    helperService.saveUser(loginDTO.getEmail(), loginDTO.getPassword(), true);

    // Set password empty
    loginDTO.setPassword("");

    // Try to log in with inactive user
    mockMvc.perform(
                    post(ApiConstants.LOGIN)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(loginDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Password space")
  void passwordSpaceTest() throws Exception {
    LoginDTO loginDTO = helperService.getLoginDTO();

    // Save valid user
    helperService.saveUser(loginDTO.getEmail(), loginDTO.getPassword(), true);

    // Set password space
    loginDTO.setPassword(" ");

    // Try to log in
    mockMvc.perform(
                    post(ApiConstants.LOGIN)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(loginDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Password too short")
  void passwordTooShortTest() throws Exception {
    LoginDTO loginDTO = helperService.getLoginDTO();

    // Save valid user
    helperService.saveUser(loginDTO.getEmail(), loginDTO.getPassword(), true);

    loginDTO.setPassword("123");

    // Try to log in
    mockMvc.perform(
                    post(ApiConstants.LOGIN)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(loginDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("404 - User inactive")
  void userInactiveTest() throws Exception {
    LoginDTO loginDTO = helperService.getLoginDTO();

    // Save inactive user
    helperService.saveUser(loginDTO.getEmail(), loginDTO.getPassword(), false);

    // Try to log in
    mockMvc.perform(
                    post(ApiConstants.LOGIN)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(loginDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isNotFound());
  }

  @Test
  @DisplayName("404 - Not found")
  void notFoundTest() throws Exception {
    LoginDTO loginDTO = helperService.getLoginDTO();

    // Try to log in
    mockMvc.perform(
                    post(ApiConstants.LOGIN)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(loginDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isNotFound());
  }

  @Test
  @DisplayName("405 - Wrong HTTP method")
  void wrongHttpMethodTest() throws Exception {
    LoginDTO loginDTO = helperService.getLoginDTO();

    // Save valid user
    helperService.saveUser(loginDTO.getEmail(), loginDTO.getPassword(), true);

    // Try to log in
    mockMvc.perform(
                    patch(ApiConstants.LOGIN)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(loginDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isMethodNotAllowed());
  }

  @Test
  @DisplayName("415 - Invalid content type")
  void invalidContentType() throws Exception {
    LoginDTO loginDTO = helperService.getLoginDTO();

    // Save valid user
    helperService.saveUser(loginDTO.getEmail(), loginDTO.getPassword(), true);

    // Try to log in
    mockMvc.perform(
                    post(ApiConstants.LOGIN)
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .content(objectMapper.writeValueAsString(loginDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isUnsupportedMediaType());
  }

}
