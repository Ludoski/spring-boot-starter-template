package com.organization.template.unit.controller.auth;

import com.organization.template.controller.auth.impl.RegisterControllerImpl;
import com.organization.template.dto.auth.RegisterDTO;
import com.organization.template.infrastructure.BaseUnitTest;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.auth.RegisterUserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

class RegisterControllerTest extends BaseUnitTest {

  @Mock
  private RegisterUserService registerUserService;

  @InjectMocks
  private RegisterControllerImpl registerController;

  @Test
  @DisplayName("Successful register")
  void registerTest() {
    User user = helperService.getUser();

    RegisterDTO registerDTO = RegisterDTO.builder()
            .email(user.getEmail())
            .password(user.getPassword())
            .username(user.getUsername())
            .firstName("first")
            .lastName("last")
            .build();

    registerController.register(registerDTO);

    verify(registerUserService).registerUser(any());
  }

}
