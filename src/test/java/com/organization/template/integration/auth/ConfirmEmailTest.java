package com.organization.template.integration.auth;

import com.organization.template.constants.ApiConstants;
import com.organization.template.infrastructure.BaseIntegrationTest;
import com.organization.template.repository.model.user.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ConfirmEmailTest extends BaseIntegrationTest {

  @Test
  @DisplayName("200 - Email confirmed")
  void successTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, false);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.CONFIRM_EMAIL, user.getActivationToken())
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.message").isEmpty());
  }

  @Test
  @DisplayName("400 - Wrong activation token format")
  void wrongActivationTokenFormatTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, false);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.CONFIRM_EMAIL, 123456)
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Activation token invalid")
  void activationTokenInvalidTest() throws Exception {
    // Check response
    mockMvc.perform(
                    patch(ApiConstants.CONFIRM_EMAIL, UUID.randomUUID())
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("405 - Wrong HTTP method")
  void wrongHttpMethodTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, false);

    // Check response
    mockMvc.perform(
                    post(ApiConstants.CONFIRM_EMAIL, user.getActivationToken())
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isMethodNotAllowed());
  }

}
