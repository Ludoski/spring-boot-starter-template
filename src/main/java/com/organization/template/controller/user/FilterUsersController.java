package com.organization.template.controller.user;

import com.organization.template.config.OpenApiConfiguration;
import com.organization.template.constants.ApiConstants;
import com.organization.template.constants.RoleConstants;
import com.organization.template.dto.user.FilterUsersDTO;
import com.organization.template.dto.user.FilteredUsersDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Filter users controller
 */
public interface FilterUsersController {

  /**
   * Filter users
   * @param filterUsersDTO           Parameters
   * @return                            All filtered users
   */
  @Tag(name = OpenApiConfiguration.USER_TAG)
  @Operation(summary = "Filter users")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Total filterable, Page of users"),
          @ApiResponse(responseCode = "400", description = "Page or size is missing or negative", content = @Content),
          @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
          @ApiResponse(responseCode = "403", description = "Not admin", content = @Content),
          @ApiResponse(responseCode = "405", description = "HTTP method must be POST", content = @Content),
          @ApiResponse(responseCode = "415", description = "Content type should be application/x-www-form-urlencoded", content = @Content)
  })
  @PreAuthorize(RoleConstants.HAS_ROLE_ADMIN)
  @PostMapping(path = ApiConstants.FILTER_ALL_USERS, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  FilteredUsersDTO filterUsers(@RequestBody @Validated FilterUsersDTO filterUsersDTO);

}
