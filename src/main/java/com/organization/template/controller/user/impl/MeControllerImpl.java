package com.organization.template.controller.user.impl;

import com.organization.template.controller.user.MeController;
import com.organization.template.dto.user.UserDTO;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.user.MeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @see MeController
 */
@RestController
public class MeControllerImpl implements MeController {

  @Autowired
  private MeService meService;

  /**
   * @see MeController#me()
   */
  @Override
  public UserDTO me() {
    User user = meService.me();
    return new UserDTO(user);
  }

}
