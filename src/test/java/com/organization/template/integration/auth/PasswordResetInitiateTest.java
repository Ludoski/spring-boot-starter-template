package com.organization.template.integration.auth;

import com.organization.template.constants.ApiConstants;
import com.organization.template.dto.auth.PasswordResetInitiateDTO;
import com.organization.template.infrastructure.BaseIntegrationTest;
import com.organization.template.repository.model.user.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class PasswordResetInitiateTest extends BaseIntegrationTest {

  @Test
  @DisplayName("200 - Password reset initiated")
  void successTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, false);

    // Prepare valid payload
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email(user.getEmail())
            .build();

    // Check response
    mockMvc.perform(
                    post(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.message").isEmpty());
  }

  @Test
  @DisplayName("400 - Request body is empty")
  void requestBodyIsEmptyTest() throws Exception {
    // Check response
    mockMvc.perform(
                    post(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(null))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - No email in request")
  void noEmailInRequestTest() throws Exception {
    // Prepare invalid payload
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email(null)
            .build();

    // Check response
    mockMvc.perform(
                    post(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Wrong email format")
  void wrongEmailFormatTest() throws Exception {
    // Prepare invalid payload
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email("email")
            .build();

    // Check response
    mockMvc.perform(
                    post(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Email empty")
  void emailEmptyTest() throws Exception {
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email("")
            .build();

    // Check response
    mockMvc.perform(
                    post(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Email space")
  void emailSpaceTest() throws Exception {
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email(" ")
            .build();

    // Check response
    mockMvc.perform(
                    post(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("404 - Not found")
  void notFoundTest() throws Exception {
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email("test@test.com")
            .build();

    // Check response
    mockMvc.perform(
                    post(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isNotFound());
  }

  @Test
  @DisplayName("405 - Wrong HTTP method")
  void wrongHttpMethodTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, false);

    // Prepare valid payload
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email(user.getEmail())
            .build();

    // Check response
    mockMvc.perform(
                    delete(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isMethodNotAllowed());
  }

  @Test
  @DisplayName("415 - Invalid content type")
  void invalidContentType() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, false);

    // Prepare valid payload
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email(user.getEmail())
            .build();

    // Check response
    mockMvc.perform(
                    post(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isUnsupportedMediaType());
  }


}
