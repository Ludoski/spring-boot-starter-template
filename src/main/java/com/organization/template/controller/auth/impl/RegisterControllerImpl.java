package com.organization.template.controller.auth.impl;

import com.organization.template.controller.auth.RegisterController;
import com.organization.template.dto.auth.RegisterDTO;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.auth.RegisterUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @see RegisterController
 */
@RestController
public class RegisterControllerImpl implements RegisterController {

  @Autowired
  private RegisterUserService registerUserService;

  /**
   * @see RegisterController#register(RegisterDTO)
   */
  @Override
  public void register(RegisterDTO registerDTO) {
    registerUserService.registerUser(new User(registerDTO));
  }

}
