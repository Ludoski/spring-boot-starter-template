package com.organization.template.service.user.impl;

import com.organization.template.exception.user.UserNotFoundException;
import com.organization.template.repository.UserRepository;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.auth.CurrentUserService;
import com.organization.template.service.user.MeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

/**
 * @see MeService
 */
@Slf4j
@Service
public class MeServiceImpl implements MeService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CurrentUserService currentUserService;

  /**
   * @see MeService#me()
   */
  @Transactional
  @Override
  public User me() {
    // Get current user id
    UUID currentUserId = currentUserService.getId();

    log.info("User ({}) is trying to fetch self.", currentUserId);

    UUID id = currentUserService.getId();

    // Get user if exists
    Optional<User> optionalUser = userRepository.findById(id);
    if (optionalUser.isEmpty()) {
      log.error("User with id ({}) not found.", id);
      throw new UserNotFoundException();
    }

    log.info("User ({}) successfully fetched self.", currentUserId);

    return optionalUser.get();
  }

}
