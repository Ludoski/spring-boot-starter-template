package com.organization.template.constants;

public class SecurityConstants {

  private SecurityConstants() {
    throw new IllegalStateException("Utility class");
  }

  public static final int ACCESS_TOKEN_DURATION_IN_MINUTES = 200;
  public static final int REFRESH_TOKEN_DURATION_IN_MINUTES = 1000;
  public static final String AUTHORITIES_CLAIM_NAME = "roles";

}
