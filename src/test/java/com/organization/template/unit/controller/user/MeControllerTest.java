package com.organization.template.unit.controller.user;

import com.organization.template.controller.user.impl.MeControllerImpl;
import com.organization.template.dto.user.UserDTO;
import com.organization.template.infrastructure.BaseUnitTest;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.user.MeService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class MeControllerTest extends BaseUnitTest {

  @Mock
  private MeService meService;

  @InjectMocks
  private MeControllerImpl meController;

  @Test
  @DisplayName("Successfully get user model for self")
  void meTest() {
    User user = helperService.getUser();

    when(meService.me())
            .thenReturn(user);

    UserDTO result = meController.me();

    assertThat(result.getEmail()).isEqualTo(user.getEmail());
  }

}
