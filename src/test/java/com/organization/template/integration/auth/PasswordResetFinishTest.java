package com.organization.template.integration.auth;

import com.organization.template.constants.ApiConstants;
import com.organization.template.dto.auth.PasswordResetFinishDTO;
import com.organization.template.infrastructure.BaseIntegrationTest;
import com.organization.template.repository.model.user.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class PasswordResetFinishTest extends BaseIntegrationTest {

  @Test
  @DisplayName("200 - Password reset finished")
  void successTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Get password reset token
    UUID token = user.getPasswordResetToken();

    // Prepare valid payload
    PasswordResetFinishDTO passwordResetFinishDTO = PasswordResetFinishDTO.builder()
            .token(token)
            .password("new password")
            .build();

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetFinishDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.message").isEmpty());
  }

  @Test
  @DisplayName("400 - Request body is empty")
  void requestBodyIsEmptyTest() throws Exception {
    // Check response
    mockMvc.perform(
                    patch(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(null))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - No token in request")
  void noEmailInRequestTest() throws Exception {
    // Prepare invalid payload
    PasswordResetFinishDTO passwordResetFinishDTO = PasswordResetFinishDTO.builder()
            .password("new password")
            .build();

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetFinishDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - No password in request")
  void noPasswordInRequestTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Get password reset token
    UUID token = user.getPasswordResetToken();

    // Prepare valid payload
    PasswordResetFinishDTO passwordResetFinishDTO = PasswordResetFinishDTO.builder()
            .token(token)
            .build();

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetFinishDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Password empty")
  void passwordEmptyTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Get password reset token
    UUID token = user.getPasswordResetToken();

    // Prepare valid payload
    PasswordResetFinishDTO passwordResetFinishDTO = PasswordResetFinishDTO.builder()
            .token(token)
            .password("")
            .build();

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetFinishDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Password space")
  void passwordSpaceTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Get password reset token
    UUID token = user.getPasswordResetToken();

    // Prepare valid payload
    PasswordResetFinishDTO passwordResetFinishDTO = PasswordResetFinishDTO.builder()
            .token(token)
            .password(" ")
            .build();

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetFinishDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Password too short")
  void passwordTooShortTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Get password reset token
    UUID token = user.getPasswordResetToken();

    // Prepare valid payload
    PasswordResetFinishDTO passwordResetFinishDTO = PasswordResetFinishDTO.builder()
            .token(token)
            .password("123")
            .build();

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetFinishDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("404 - Not found")
  void notFoundTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    helperService.saveUser(email, password, true);

    // Get password reset token
    UUID token = UUID.randomUUID();

    // Prepare valid payload
    PasswordResetFinishDTO passwordResetFinishDTO = PasswordResetFinishDTO.builder()
            .token(token)
            .password("12345678")
            .build();

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetFinishDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isNotFound());
  }

  @Test
  @DisplayName("405 - Wrong HTTP method")
  void wrongHttpMethodTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Get password reset token
    UUID token = user.getPasswordResetToken();

    // Prepare valid payload
    PasswordResetFinishDTO passwordResetFinishDTO = PasswordResetFinishDTO.builder()
            .token(token)
            .password("new password")
            .build();

    // Check response
    mockMvc.perform(
                    delete(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetFinishDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isMethodNotAllowed());
  }

  @Test
  @DisplayName("415 - Invalid content type")
  void invalidContentType() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Get password reset token
    UUID token = user.getPasswordResetToken();

    // Prepare valid payload
    PasswordResetFinishDTO passwordResetFinishDTO = PasswordResetFinishDTO.builder()
            .token(token)
            .password("new password")
            .build();

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.PASSWORD_RESET)
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .content(objectMapper.writeValueAsString(passwordResetFinishDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isUnsupportedMediaType());
  }


}
