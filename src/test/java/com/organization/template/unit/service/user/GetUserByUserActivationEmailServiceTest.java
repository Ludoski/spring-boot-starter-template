package com.organization.template.unit.service.user;

import com.organization.template.exception.user.UserNotFoundException;
import com.organization.template.infrastructure.BaseUnitTest;
import com.organization.template.repository.UserRepository;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.auth.CurrentUserService;
import com.organization.template.service.user.impl.GetUserByEmailServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

class GetUserByUserActivationEmailServiceTest extends BaseUnitTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private CurrentUserService currentUserService;

  @InjectMocks
  private GetUserByEmailServiceImpl getUserByEmailService;

  @Test
  @DisplayName("Successfully get user")
  void getUserTest() {
    User user = helperService.getUser();

    doReturn(Optional.of(user))
            .when(userRepository).findByEmail(any());

    User result = getUserByEmailService.getUserByEmail(user.getEmail());

    assertThat(result.getEmail()).isEqualTo(user.getEmail());
  }

  @Test
  @DisplayName("User not found")
  void userNotFoundTest() {
    User user = helperService.getUser();
    String userEmail = user.getEmail();

    assertThatThrownBy(() -> getUserByEmailService.getUserByEmail(userEmail))
            .isInstanceOf(UserNotFoundException.class);
  }

}
