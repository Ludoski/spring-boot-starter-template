package com.organization.template.unit.controller.auth;

import com.organization.template.controller.auth.impl.RefreshTokenControllerImpl;
import com.organization.template.dto.auth.TokensDTO;
import com.organization.template.infrastructure.BaseUnitTest;
import com.organization.template.service.auth.RefreshTokenService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class RefreshTokenControllerTest extends BaseUnitTest {

  @Mock
  private RefreshTokenService refreshTokenService;

  @InjectMocks
  private RefreshTokenControllerImpl refreshTokenController;

  @Test
  @DisplayName("Successful refresh tokens")
  void refreshTokenTest() {
    TokensDTO tokensDTO = helperService.getTokens();

    when(refreshTokenService.refreshToken(any()))
            .thenReturn(tokensDTO);

    TokensDTO result = refreshTokenController.refreshToken(any());

    assertThat(result).isEqualTo(tokensDTO);
  }

}
