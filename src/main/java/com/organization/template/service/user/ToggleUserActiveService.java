package com.organization.template.service.user;

import com.organization.template.repository.model.user.User;

import java.util.UUID;

/**
 * Service for toggling user active
 */
public interface ToggleUserActiveService {

  /**
   * Toggle user active
   * @param id          User id
   * @return            User model
   */
  User toggleUserActive(UUID id);

}
