package com.organization.template.service.user.impl;

import com.organization.template.exception.auth.ForbiddenException;
import com.organization.template.exception.user.UserNotFoundException;
import com.organization.template.model.enums.UserRole;
import com.organization.template.repository.UserRepository;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.auth.CurrentUserService;
import com.organization.template.service.user.DeleteUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

/**
 * @see DeleteUserService
 */
@Slf4j
@Service
public class DeleteUserServiceImpl implements DeleteUserService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CurrentUserService currentUserService;

  /**
   * @see DeleteUserService#deleteUser(UUID)
   */
  @Transactional
  @Override
  public void deleteUser(UUID id) {
    // Get current user id
    UUID currentUserId = currentUserService.getId();

    log.info("User ({}) is trying to delete user with id ({}). ", currentUserId, id);

    // Return if user is not admin and want to delete other
    if (!currentUserService.hasAuthority(UserRole.ROLE_ADMIN) &&
            !currentUserId.equals(id)) {
      log.error("User ({}) is forbidden to delete user ({})", currentUserId, id);
      throw new ForbiddenException();
    }

    // Get user if exists
    Optional<User> optionalUser = userRepository.findById(id);
    if (optionalUser.isEmpty()) {
      log.error("User with id ({}) not found.", id);
      throw new UserNotFoundException();
    }
    User user = optionalUser.get();

    // If trying to delete super admin
    if (user.hasRole(UserRole.ROLE_SUPER_ADMIN)) {
      log.error("User ({}) is forbidden to delete super admin ({})", currentUserId, id);
      throw new ForbiddenException();
    }

    // Delete user
    userRepository.delete(user);

    log.info("User ({}) successfully deleted user with id ({}).", currentUserId, id);
  }

}
