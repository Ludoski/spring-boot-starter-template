package com.organization.template.controller.auth.impl;

import com.organization.template.controller.auth.ConfirmEmailController;
import com.organization.template.service.auth.ConfirmEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @see ConfirmEmailController
 */
@RestController
public class ConfirmEmailControllerImpl implements ConfirmEmailController {

  @Autowired
  private ConfirmEmailService confirmEmailService;

  /**
   * @see ConfirmEmailController#confirmEmail(UUID)
   */
  @Override
  public void confirmEmail(UUID activationToken) {
    confirmEmailService.confirmEmail(activationToken);
  }

}
