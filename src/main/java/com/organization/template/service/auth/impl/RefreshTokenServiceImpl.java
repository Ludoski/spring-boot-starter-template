package com.organization.template.service.auth.impl;

import com.organization.template.dto.auth.TokensDTO;
import com.organization.template.exception.auth.UnauthorizedException;
import com.organization.template.exception.user.UserNotFoundException;
import com.organization.template.service.auth.JwtTokenService;
import com.organization.template.service.auth.RefreshTokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

/**
 * @see RefreshTokenService
 */
@Slf4j
@Service
public class RefreshTokenServiceImpl implements RefreshTokenService {

  @Autowired
  private JwtTokenService jwtTokenService;

  @Autowired
  private UserDetailsService userDetailsService;

  /**
   * @see RefreshTokenService#refreshToken(HttpServletRequest)
   */
  @Transactional
  @Override
  public TokensDTO refreshToken(HttpServletRequest httpServletRequest) {
    Optional<String> token = Optional.ofNullable(httpServletRequest.getHeader(AUTHORIZATION)); //Authorization: Bearer TOKEN
    if (token.isEmpty()) {
      log.error("Token is empty.");
      throw new UnauthorizedException();
    }

    String username = jwtTokenService.decodeToken(token.get()).get("username");
    if (username == null) {
      log.error("No username in token.");
      throw new UnauthorizedException();
    }

    log.info("Trying to refresh access token for user ({})", username);
    // Get user if exists
    UserDetails userDetails;
    try {
      userDetails = userDetailsService.loadUserByUsername(username);
    } catch (UsernameNotFoundException e) {
      log.error("User ({}) not found.", username, e);
      throw new UserNotFoundException();
    }

    String accessToken = jwtTokenService.createJwtAccessToken(userDetails);

    log.info("Successfully refreshed token for user ({}). Returning access token.", username);
    return new TokensDTO(accessToken);
  }

}
