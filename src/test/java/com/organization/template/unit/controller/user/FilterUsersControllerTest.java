package com.organization.template.unit.controller.user;

import com.organization.template.controller.user.impl.FilterUsersControllerImpl;
import com.organization.template.dto.user.FilterUsersDTO;
import com.organization.template.dto.user.FilteredUsersDTO;
import com.organization.template.infrastructure.BaseUnitTest;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.user.CountFilterableUsersService;
import com.organization.template.service.user.GetPageOfFilteredUsersService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class FilterUsersControllerTest extends BaseUnitTest {

  @Mock
  private CountFilterableUsersService countFilterableUsersService;

  @Mock
  private GetPageOfFilteredUsersService getPageOfFilteredUsersService;

  @InjectMocks
  private FilterUsersControllerImpl filterAllUsersController;

  @Test
  @DisplayName("Filter all users")
  void filterAllUsersTest() {
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter("check")
            .page(0)
            .size(1)
            .build();
    List<User> users = List.of(helperService.getUser());

    when(countFilterableUsersService.countFilterableUsers(filterUsersDTO))
            .thenReturn((long) users.size());
    when(getPageOfFilteredUsersService.filterUsers(filterUsersDTO))
            .thenReturn(users);

    FilteredUsersDTO result = filterAllUsersController.filterUsers(filterUsersDTO);

    assertThat(result.getTotal()).isEqualTo(1);
    assertThat(result.getUsers().get(0).getEmail()).isEqualTo(users.get(0).getEmail());
  }

}
