package com.organization.template.controller.user.impl;

import com.organization.template.controller.user.FilterUsersController;
import com.organization.template.dto.user.FilterUsersDTO;
import com.organization.template.dto.user.FilteredUsersDTO;
import com.organization.template.dto.user.UserDTO;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.user.CountFilterableUsersService;
import com.organization.template.service.user.GetPageOfFilteredUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @see FilterUsersController
 */
@RestController
public class FilterUsersControllerImpl implements FilterUsersController {

  @Autowired
  private CountFilterableUsersService countFilterableUsersService;

  @Autowired
  private GetPageOfFilteredUsersService getPageOfFilteredUsersService;

  /**
   * @see FilterUsersController#filterUsers(FilterUsersDTO)
   */
  @Transactional
  @Override
  public FilteredUsersDTO filterUsers(FilterUsersDTO filterUsersDTO) {
    // Count filterable users
    long numberOfFilterableUsers = countFilterableUsersService.countFilterableUsers(filterUsersDTO);

    // Get page of users
    List<User> users = getPageOfFilteredUsersService.filterUsers(filterUsersDTO);

    // Transform db models to DTO models
    List<UserDTO> usersDTOS = users.stream()
            .map(UserDTO::new)
            .collect(Collectors.toList());

    return FilteredUsersDTO.builder()
            .total(numberOfFilterableUsers)
            .users(usersDTOS)
            .build();
  }

}
