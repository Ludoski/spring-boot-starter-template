package com.organization.template.service.user;

import com.organization.template.dto.user.FilterUsersDTO;

/**
 * Count filterable users service
 */
public interface CountFilterableUsersService {

  /**
   * Count filterable users
   * @param filterUsersDTO              Parameters
   * @return                            Number of users
   */
  long countFilterableUsers(FilterUsersDTO filterUsersDTO);

}
