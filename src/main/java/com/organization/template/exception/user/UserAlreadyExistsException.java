package com.organization.template.exception.user;

import com.organization.template.exception.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * User already exists exception
 */
public class UserAlreadyExistsException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "User already exists.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.BAD_REQUEST;

  public UserAlreadyExistsException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
