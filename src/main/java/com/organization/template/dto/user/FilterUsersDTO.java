package com.organization.template.dto.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

/**
 * Filter users DTO
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FilterUsersDTO {

  @JsonProperty
  private String filter;

  @NotNull
  @PositiveOrZero
  private Integer page;

  @NotNull
  @PositiveOrZero
  private Integer size;

}
