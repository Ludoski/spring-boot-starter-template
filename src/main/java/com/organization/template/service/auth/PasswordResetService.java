package com.organization.template.service.auth;

import com.organization.template.dto.auth.PasswordResetFinishDTO;
import com.organization.template.dto.auth.PasswordResetInitiateDTO;

/**
 * Reset password service
 */
public interface PasswordResetService {

  /**
   * Initiate password reset
   * @param passwordResetInitiateDTO       Password reset initiate payload
   */
  void passwordResetInitiate(PasswordResetInitiateDTO passwordResetInitiateDTO);

  /**
   * Finish password reset
   * @param passwordResetFinishDTO         Password reset finish payload
   */
  void passwordResetFinish(PasswordResetFinishDTO passwordResetFinishDTO);

}
