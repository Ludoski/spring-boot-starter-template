package com.organization.template.repository;

import com.organization.template.model.enums.UserRole;
import com.organization.template.repository.model.user.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

/**
 * Role repository
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, UUID> {

  /**
   * Find role by slug
   * @param userRole       User role
   * @return               Optional role
   */
  Optional<Role> findBySlug(UserRole userRole);

}
