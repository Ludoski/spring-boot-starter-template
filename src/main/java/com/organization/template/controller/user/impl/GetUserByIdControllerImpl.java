package com.organization.template.controller.user.impl;

import com.organization.template.controller.user.GetUserByIdController;
import com.organization.template.dto.user.UserDTO;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.user.GetUserByIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @see GetUserByIdController
 */
@RestController
public class GetUserByIdControllerImpl implements GetUserByIdController {

  @Autowired
  private GetUserByIdService getUserByIdService;

  /**
   * @see GetUserByIdController#getUserById(UUID)
   */
  @Override
  public UserDTO getUserById(UUID id) {
    User user = getUserByIdService.getUserById(id);
    return new UserDTO(user);
  }

}
