package com.organization.template.controller.auth;

import com.organization.template.config.OpenApiConfiguration;
import com.organization.template.constants.ApiConstants;
import com.organization.template.dto.auth.RegisterDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Create user controller
 */
public interface RegisterController {

  /**
   * Create user
   * @param registerDTO       User information
   */
  @Tag(name = OpenApiConfiguration.AUTH_TAG)
  @Operation(summary = "Register user")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "User registered"),
          @ApiResponse(responseCode = "400", description = "Request body, email, username or password is invalid", content = @Content),
          @ApiResponse(responseCode = "405", description = "HTTP method must be POST", content = @Content),
          @ApiResponse(responseCode = "415", description = "Content type should be application/json", content = @Content)
  })
  @PostMapping(path = ApiConstants.REGISTER, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  void register(@RequestBody @Validated RegisterDTO registerDTO);

}
