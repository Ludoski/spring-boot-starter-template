package com.organization.template.controller.auth.impl;

import com.organization.template.controller.auth.RefreshTokenController;
import com.organization.template.dto.auth.TokensDTO;
import com.organization.template.service.auth.RefreshTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @see RefreshTokenController
 */
@RestController
public class RefreshTokenControllerImpl implements RefreshTokenController {

  @Autowired
  private RefreshTokenService refreshTokenService;

  /**
   * @see RefreshTokenController#refreshToken(HttpServletRequest)
   */
  @Override
  public TokensDTO refreshToken(HttpServletRequest httpServletRequest) {
    return refreshTokenService.refreshToken(httpServletRequest);
  }

}
