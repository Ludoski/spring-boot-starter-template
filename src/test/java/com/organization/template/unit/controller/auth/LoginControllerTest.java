package com.organization.template.unit.controller.auth;

import com.organization.template.controller.auth.impl.LoginControllerImpl;
import com.organization.template.dto.auth.LoginDTO;
import com.organization.template.dto.auth.TokensDTO;
import com.organization.template.infrastructure.BaseUnitTest;
import com.organization.template.service.auth.LoginService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class LoginControllerTest extends BaseUnitTest {

  @Mock
  private LoginService loginService;

  @InjectMocks
  private LoginControllerImpl loginController;

  @Test
  @DisplayName("Successful login")
  void loginTest() {
    LoginDTO loginDTO = helperService.getLoginDTO();
    TokensDTO tokensDTO = helperService.getTokens();

    when(loginService.login(any(), any()))
            .thenReturn(tokensDTO);

    TokensDTO result = loginController.login(loginDTO);

    assertThat(result).isEqualTo(tokensDTO);
  }

}
