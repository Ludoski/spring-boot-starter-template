package com.organization.template.service.auth;

import java.util.UUID;

/**
 * Email confirmation service
 */
public interface ConfirmEmailService {

  /**
   * Confirm user email
   * @param activationToken         Token generated at registration
   */
  void confirmEmail(UUID activationToken);

}
