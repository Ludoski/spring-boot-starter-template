package com.organization.template.controller.auth;

import com.organization.template.config.OpenApiConfiguration;
import com.organization.template.constants.ApiConstants;
import com.organization.template.constants.RoleConstants;
import com.organization.template.dto.auth.ChangePasswordDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Change user password controller
 */
public interface ChangePasswordController {

  /**
   * Change password
   * @param changePasswordDTO              Old and new passwords
   */
  @Tag(name = OpenApiConfiguration.AUTH_TAG)
  @Operation(summary = "Change password")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Password changed"),
          @ApiResponse(responseCode = "400", description = "Empty body or password is invalid", content = @Content),
          @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
          @ApiResponse(responseCode = "404", description = "User not found", content = @Content),
          @ApiResponse(responseCode = "405", description = "HTTP method must be PATCH", content = @Content),
          @ApiResponse(responseCode = "415", description = "Content type should be application/json", content = @Content)
  })
  @PreAuthorize(RoleConstants.HAS_ROLE_USER)
  @PatchMapping(path = ApiConstants.CHANGE_PASSWORD, consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  void changePassword(@RequestBody @Validated ChangePasswordDTO changePasswordDTO);

}
