package com.organization.template.dto.auth;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.organization.template.constants.MiscConstants;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Create user DTO
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RegisterDTO {

  @Email
  @NotBlank
  @NotNull
  @JsonProperty
  private String email;

  @Size(min = MiscConstants.PASSWORD_MIN_LENGTH)
  @NotBlank
  @NotNull
  @JsonProperty
  private String password;

  @NotBlank
  @NotNull
  @JsonProperty
  private String username;

  @JsonProperty("first_name")
  private String firstName;

  @JsonProperty("last_name")
  private String lastName;

}
