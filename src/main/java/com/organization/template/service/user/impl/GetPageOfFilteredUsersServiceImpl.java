package com.organization.template.service.user.impl;

import com.organization.template.dto.user.FilterUsersDTO;
import com.organization.template.repository.UserRepository;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.auth.CurrentUserService;
import com.organization.template.service.user.GetPageOfFilteredUsersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * @see GetPageOfFilteredUsersService
 */
@Slf4j
@Service
public class GetPageOfFilteredUsersServiceImpl implements GetPageOfFilteredUsersService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CurrentUserService currentUserService;

  /**
   * @see GetPageOfFilteredUsersService#filterUsers(FilterUsersDTO)
   */
  @Transactional
  @Override
  public List<User> filterUsers(FilterUsersDTO filterUsersDTO) {
    // Get current user id
    UUID currentUserId = currentUserService.getId();

    String filter = filterUsersDTO.getFilter();
    if (filter == null || filter.trim().isBlank()) {
      filter = "";
    }
    int page = filterUsersDTO.getPage();
    int size = filterUsersDTO.getSize();

    log.info("User ({}) is trying to filter users by filter ({}), page ({}) and size ({}).",
            currentUserId, filter, page, size);

    // Filter all users from database
    Pageable pageable = PageRequest.of(page, size);
    Page<User> users = userRepository.findAllByEmailContainingIgnoreCaseOrUsernameContainingIgnoreCaseOrFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(filter, filter, filter, filter, pageable);

    log.info("User ({}) successfully filtered users by filter ({}), page ({}) and size ({}).",
            currentUserId, filter, page, size);

    return users.getContent();
  }

}
