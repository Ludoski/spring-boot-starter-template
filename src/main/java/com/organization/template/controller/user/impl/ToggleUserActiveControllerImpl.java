package com.organization.template.controller.user.impl;

import com.organization.template.controller.user.ToggleUserActiveController;
import com.organization.template.dto.user.UserDTO;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.user.ToggleUserActiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @see ToggleUserActiveController
 */
@RestController
public class ToggleUserActiveControllerImpl implements ToggleUserActiveController {

  @Autowired
  private ToggleUserActiveService toggleUserActiveService;

  /**
   * @see ToggleUserActiveController#toggleUserActive(UUID)
   */
  @Override
  public UserDTO toggleUserActive(UUID id) {
    User user = toggleUserActiveService.toggleUserActive(id);
    return new UserDTO(user);
  }

}
