package com.organization.template.controller.user.impl;

import com.organization.template.controller.user.UpdateUserController;
import com.organization.template.dto.user.UpdateUserDTO;
import com.organization.template.dto.user.UserDTO;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.user.UpdateUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @see UpdateUserController
 */
@RestController
public class UpdateUserControllerImpl implements UpdateUserController {

  @Autowired
  private UpdateUserService updateUserService;

  /**
   * @see UpdateUserController#updateUser(UUID, UpdateUserDTO)
   */
  @Override
  public UserDTO updateUser(UUID id, UpdateUserDTO updateUserDTO) {
    User user = updateUserService.updateUser(id, new User(updateUserDTO));
    return new UserDTO(user);
  }

}
