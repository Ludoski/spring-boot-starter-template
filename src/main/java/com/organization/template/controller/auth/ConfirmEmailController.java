package com.organization.template.controller.auth;

import com.organization.template.config.OpenApiConfiguration;
import com.organization.template.constants.ApiConstants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

/**
 * Confirm user email by activation token
 */
public interface ConfirmEmailController {

  /**
   * Confirm email
   * @param activationToken       Activation token
   */
  @Tag(name = OpenApiConfiguration.AUTH_TAG)
  @Operation(summary = "Confirm email")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Email confirmed"),
          @ApiResponse(responseCode = "400", description = "Activation token must be UUID or activation token is invalid", content = @Content),
          @ApiResponse(responseCode = "405", description = "HTTP method must be PATCH", content = @Content)
  })
  @PatchMapping(path = ApiConstants.CONFIRM_EMAIL)
  @ResponseBody
  void confirmEmail(@PathVariable("activation-token") UUID activationToken);

}
