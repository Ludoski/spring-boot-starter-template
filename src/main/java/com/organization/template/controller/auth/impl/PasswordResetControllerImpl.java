package com.organization.template.controller.auth.impl;

import com.organization.template.controller.auth.PasswordResetController;
import com.organization.template.dto.auth.PasswordResetFinishDTO;
import com.organization.template.dto.auth.PasswordResetInitiateDTO;
import com.organization.template.service.auth.PasswordResetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @see PasswordResetController
 */
@RestController
public class PasswordResetControllerImpl implements PasswordResetController {

  @Autowired
  private PasswordResetService passwordResetService;

  /**
   * @see PasswordResetController#passwordResetInitiate(PasswordResetInitiateDTO)
   */
  @Override
  public void passwordResetInitiate(PasswordResetInitiateDTO passwordResetInitiateDTO) {
    passwordResetService.passwordResetInitiate(passwordResetInitiateDTO);
  }

  /**
   * @see PasswordResetController#passwordResetFinish(PasswordResetFinishDTO)
   */
  @Override
  public void passwordResetFinish(PasswordResetFinishDTO passwordResetFinishDTO) {
    passwordResetService.passwordResetFinish(passwordResetFinishDTO);
  }

}
