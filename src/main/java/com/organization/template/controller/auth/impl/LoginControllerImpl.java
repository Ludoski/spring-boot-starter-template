package com.organization.template.controller.auth.impl;

import com.organization.template.controller.auth.LoginController;
import com.organization.template.dto.auth.LoginDTO;
import com.organization.template.dto.auth.TokensDTO;
import com.organization.template.service.auth.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @see LoginController
 */
@RestController
public class LoginControllerImpl implements LoginController {

  @Autowired
  private LoginService loginService;

  /**
   * @see LoginController#login(LoginDTO)
   */
  @Override
  public TokensDTO login(LoginDTO loginDTO) {
    return loginService.login(loginDTO.getEmail(), loginDTO.getPassword());
  }

}
