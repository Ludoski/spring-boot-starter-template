package com.organization.template.unit.controller.user;

import com.organization.template.controller.user.impl.UpdateUserControllerImpl;
import com.organization.template.dto.user.UpdateUserDTO;
import com.organization.template.dto.user.UserDTO;
import com.organization.template.infrastructure.BaseUnitTest;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.user.UpdateUserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class UpdateUserControllerTest extends BaseUnitTest {

  @Mock
  private UpdateUserService updateUserService;

  @InjectMocks
  private UpdateUserControllerImpl updateUserController;

  @Test
  @DisplayName("Successfully update user")
  void updateUserTest() {
    UpdateUserDTO updateUserDTO = UpdateUserDTO.builder()
            .email(helperService.getRandomEmail())
            .build();
    User user = helperService.getUser();
    user.setEmail(updateUserDTO.getEmail());

    when(updateUserService.updateUser(any(), any()))
            .thenReturn(user);

    UserDTO result = updateUserController.updateUser(user.getId(), updateUserDTO);

    assertThat(result.getEmail()).isEqualTo(user.getEmail());
  }

}
