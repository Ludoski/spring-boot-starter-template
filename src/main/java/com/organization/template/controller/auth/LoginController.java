package com.organization.template.controller.auth;

import com.organization.template.config.OpenApiConfiguration;
import com.organization.template.constants.ApiConstants;
import com.organization.template.dto.auth.LoginDTO;
import com.organization.template.dto.auth.TokensDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * User login controller
 */
public interface LoginController {

  /**
   * User login
   * @param loginDTO          Login DTO
   * @return                  Access and refresh tokens
   */
  @Tag(name = OpenApiConfiguration.AUTH_TAG)
  @Operation(summary = "User login")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Successful login"),
          @ApiResponse(responseCode = "400", description = "Empty body, email or password is missing, email in invalid format or password too short", content = @Content),
          @ApiResponse(responseCode = "404", description = "User not found or not active", content = @Content),
          @ApiResponse(responseCode = "405", description = "HTTP method must be POST", content = @Content),
          @ApiResponse(responseCode = "415", description = "Content type should be application/x-www-form-urlencoded", content = @Content)
  })
  @PostMapping(path = ApiConstants.LOGIN,
          consumes = MediaType.APPLICATION_JSON_VALUE,
          produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  TokensDTO login(@RequestBody @Validated LoginDTO loginDTO);

}
