package com.organization.template.service.user;

import java.util.UUID;

/**
 * Delete user by id service
 */
public interface DeleteUserService {

  /**
   * Delete user by id
   * @param id        User id
   */
  void deleteUser(UUID id);

}
