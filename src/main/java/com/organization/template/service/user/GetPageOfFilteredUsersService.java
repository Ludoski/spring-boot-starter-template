package com.organization.template.service.user;

import com.organization.template.dto.user.FilterUsersDTO;
import com.organization.template.repository.model.user.User;

import java.util.List;

/**
 * Filter users service
 */
public interface GetPageOfFilteredUsersService {

  /**
   * Filter users
   * @param filterUsersDTO              Parameters
   * @return                            List of all users
   */
  List<User> filterUsers(FilterUsersDTO filterUsersDTO);

}
