package com.organization.template.service.user.impl;

import com.organization.template.exception.auth.ForbiddenException;
import com.organization.template.exception.user.UserNotFoundException;
import com.organization.template.model.enums.UserRole;
import com.organization.template.repository.UserRepository;
import com.organization.template.repository.model.user.User;
import com.organization.template.service.auth.CurrentUserService;
import com.organization.template.service.user.ToggleUserActiveService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Optional;
import java.util.UUID;

/**
 * @see ToggleUserActiveService
 */
@Slf4j
@Service
public class ToggleUserActiveServiceImpl implements ToggleUserActiveService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CurrentUserService currentUserService;

  /**
   * @see ToggleUserActiveService#toggleUserActive(UUID)
   */
  @Transactional
  @Override
  public User toggleUserActive(UUID id) {
    // Get current user id
    UUID currentUserId = currentUserService.getId();

    log.info("User ({}) is trying to toggle active for user with id ({}).", currentUserId, id);

    // Check if user exists
    Optional<User> optionalUser = userRepository.findById(id);
    if (optionalUser.isEmpty()) {
      log.error("User ({}) not found.", id);
      throw new UserNotFoundException();
    }

    User user = optionalUser.get();

    // Supper admins can not be deactivated
    if (user.hasRole(UserRole.ROLE_SUPER_ADMIN)) {
      log.error("User ({}) is forbidden to toggle active super admin ({})", currentUserId, id);
      throw new ForbiddenException();
    }

    // Enable/disable
    user.setActive(!user.getActive());

    if (Boolean.TRUE.equals(user.getActive())) {
      // If user is active delete "deactivated at"
      user.setDeactivatedAt(null);
      log.info("User ({}) activated user with id ({}).", currentUserId, id);
    } else {
      // Set user deactivated time in UTC
      Timestamp now = Timestamp.valueOf(Instant.now().atOffset(ZoneOffset.UTC).toLocalDateTime());
      user.setDeactivatedAt(now);
      log.info("User ({}) deactivated user with id ({}).", currentUserId, id);
    }

    return user;
  }

}
