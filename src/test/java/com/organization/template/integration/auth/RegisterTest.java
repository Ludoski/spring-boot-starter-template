package com.organization.template.integration.auth;

import com.organization.template.constants.ApiConstants;
import com.organization.template.dto.auth.RegisterDTO;
import com.organization.template.infrastructure.BaseIntegrationTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class RegisterTest extends BaseIntegrationTest {

  @Test
  @DisplayName("200 - Register successful")
  void registerSuccessfulTest() throws Exception {
    RegisterDTO registerDTO = RegisterDTO.builder()
            .email(helperService.getRandomEmail())
            .password(helperService.getRandomString())
            .username(helperService.getRandomString())
            .build();

    // Register and check response
    mockMvc.perform(
                    post(ApiConstants.REGISTER)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(registerDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.message").isEmpty());
  }

  @Test
  @DisplayName("400 - Invalid request body")
  void invalidRequestBodyTest() throws Exception {
    mockMvc.perform(
                    post(ApiConstants.REGISTER)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(null))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Invalid email format")
  void invalidEmailFormatTest() throws Exception {
    RegisterDTO registerDTO = RegisterDTO.builder()
            .email(helperService.getRandomString())
            .password(helperService.getRandomString())
            .username(helperService.getRandomString())
            .build();

    mockMvc.perform(
                    post(ApiConstants.REGISTER)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(registerDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - No email in request")
  void noEmailInRequestTest() throws Exception {
    RegisterDTO registerDTO = RegisterDTO.builder()
            .password(helperService.getRandomString())
            .username(helperService.getRandomString())
            .build();

    mockMvc.perform(
                    post(ApiConstants.REGISTER)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(registerDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Email is empty")
  void emailIsEmptyTest() throws Exception {
    RegisterDTO registerDTO = RegisterDTO.builder()
            .email("")
            .password(helperService.getRandomString())
            .username(helperService.getRandomString())
            .build();

    mockMvc.perform(
                    post(ApiConstants.REGISTER)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(registerDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Email is space")
  void emailIsSpaceTest() throws Exception {
    RegisterDTO registerDTO = RegisterDTO.builder()
            .email(" ")
            .password(helperService.getRandomString())
            .username(helperService.getRandomString())
            .build();

    mockMvc.perform(
                    post(ApiConstants.REGISTER)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(registerDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - No password in request")
  void noPasswordInRequestTest() throws Exception {
    RegisterDTO registerDTO = RegisterDTO.builder()
            .email(helperService.getRandomEmail())
            .username(helperService.getRandomString())
            .build();

    mockMvc.perform(
                    post(ApiConstants.REGISTER)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(registerDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Password is empty")
  void passwordIsEmptyTest() throws Exception {
    RegisterDTO registerDTO = RegisterDTO.builder()
            .email(helperService.getRandomEmail())
            .password("")
            .username(helperService.getRandomString())
            .build();

    mockMvc.perform(
                    post(ApiConstants.REGISTER)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(registerDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Password is space")
  void passwordIsSpaceTest() throws Exception {
    RegisterDTO registerDTO = RegisterDTO.builder()
            .email(helperService.getRandomEmail())
            .password(" ")
            .username(helperService.getRandomString())
            .build();

    mockMvc.perform(
                    post(ApiConstants.REGISTER)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(registerDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - No username in request")
  void noUsernameInRequestTest() throws Exception {
    RegisterDTO registerDTO = RegisterDTO.builder()
            .email(helperService.getRandomEmail())
            .password(helperService.getRandomString())
            .build();

    mockMvc.perform(
                    post(ApiConstants.REGISTER)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(registerDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Username is empty")
  void usernameIsEmptyTest() throws Exception {
    RegisterDTO registerDTO = RegisterDTO.builder()
            .email(helperService.getRandomEmail())
            .password(helperService.getRandomString())
            .username("")
            .build();

    mockMvc.perform(
                    post(ApiConstants.REGISTER)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(registerDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }
  @Test
  @DisplayName("400 - Username is space")
  void usernameIsSpaceTest() throws Exception {
    RegisterDTO registerDTO = RegisterDTO.builder()
            .email(helperService.getRandomEmail())
            .password(helperService.getRandomString())
            .username(" ")
            .build();

    mockMvc.perform(
                    post(ApiConstants.REGISTER)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(registerDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("405 - Wrong HTTP method")
  void wrongHttpMethodTest() throws Exception {
    RegisterDTO registerDTO = RegisterDTO.builder()
            .email(helperService.getRandomEmail())
            .password(helperService.getRandomString())
            .username(helperService.getRandomString())
            .build();

    // Register and check response
    mockMvc.perform(
                    get(ApiConstants.REGISTER)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(registerDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isMethodNotAllowed());
  }

  @Test
  @DisplayName("415 - Invalid content type")
  void invalidContentType() throws Exception {
    RegisterDTO registerDTO = RegisterDTO.builder()
            .email(helperService.getRandomEmail())
            .password(helperService.getRandomString())
            .username(helperService.getRandomString())
            .build();

    mockMvc.perform(
                    post(ApiConstants.REGISTER)
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .content(objectMapper.writeValueAsString(registerDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isUnsupportedMediaType());
  }

}
