package com.organization.template.controller.auth;

import com.organization.template.config.OpenApiConfiguration;
import com.organization.template.constants.ApiConstants;
import com.organization.template.dto.auth.PasswordResetFinishDTO;
import com.organization.template.dto.auth.PasswordResetInitiateDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Password reset initiate
 */
public interface PasswordResetController {

  /**
   * Password reset initiate
   * @param passwordResetInitiateDTO       Password reset initiate parameters
   */
  @Tag(name = OpenApiConfiguration.AUTH_TAG)
  @Operation(summary = "Password reset initiate")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Password reset initiated"),
          @ApiResponse(responseCode = "400", description = "Empty body or email format is invalid", content = @Content),
          @ApiResponse(responseCode = "404", description = "User not found", content = @Content),
          @ApiResponse(responseCode = "405", description = "HTTP method must be POST", content = @Content),
          @ApiResponse(responseCode = "415", description = "Content type should be application/json", content = @Content)
  })
  @PostMapping(path = ApiConstants.PASSWORD_RESET, consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  void passwordResetInitiate(@RequestBody @Validated PasswordResetInitiateDTO passwordResetInitiateDTO);

  /**
   * Password reset finish
   * @param passwordResetFinishDTO       Password reset finish parameters
   */
  @Tag(name = OpenApiConfiguration.AUTH_TAG)
  @Operation(summary = "Password reset finish")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Password reset finished"),
          @ApiResponse(responseCode = "400", description = "Empty body, token or password is invalid", content = @Content),
          @ApiResponse(responseCode = "404", description = "User not found", content = @Content),
          @ApiResponse(responseCode = "405", description = "HTTP method must be PATCH", content = @Content),
          @ApiResponse(responseCode = "415", description = "Content type should be application/json", content = @Content)
  })
  @PatchMapping(path = ApiConstants.PASSWORD_RESET, consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  void passwordResetFinish(@RequestBody @Validated PasswordResetFinishDTO passwordResetFinishDTO);

}
