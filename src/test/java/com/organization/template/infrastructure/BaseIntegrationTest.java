package com.organization.template.infrastructure;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.organization.template.constants.ApiConstants;
import com.organization.template.dto.auth.LoginDTO;
import com.organization.template.dto.auth.TokensDTO;
import com.organization.template.util.response.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
public class BaseIntegrationTest {

  protected final ObjectMapper objectMapper = new ObjectMapper();

  @Autowired
  protected IntegrationHelperService helperService;

  @Autowired
  protected MockMvc mockMvc;

  protected static final PostgreSQLContainer postgreSQLContainer;

  static {
    postgreSQLContainer = (PostgreSQLContainer) new PostgreSQLContainer("postgres:latest")
            .withDatabaseName("test")
            .withUsername("postgres")
            .withPassword("postgres")
            .withReuse(true);

    postgreSQLContainer.start();
  }

  @DynamicPropertySource
  protected static void datasourceConfig(DynamicPropertyRegistry registry) {
    registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
    registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
    registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
  }

  protected TokensDTO login(String email, String password) throws Exception {
    LoginDTO loginDTO = LoginDTO.builder()
            .email(email)
            .password(password)
            .build();
    ResultActions result
            = mockMvc.perform(
                    post(ApiConstants.LOGIN)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(loginDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.message.access_token").isNotEmpty())
            .andExpect(jsonPath("$.message.refresh_token").isNotEmpty());

    String resultString = result.andReturn().getResponse().getContentAsString();
    ResponseBody responseBody = objectMapper.readValue(resultString, ResponseBody.class);
    return objectMapper.convertValue(responseBody.getMessage(), TokensDTO.class);
  }

}
