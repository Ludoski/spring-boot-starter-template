package com.organization.template.service.auth.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.organization.template.constants.SecurityConstants;
import com.organization.template.exception.auth.UnauthorizedException;
import com.organization.template.model.auth.UserPrincipal;
import com.organization.template.service.auth.JwtTokenService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @see JwtTokenService
 */
@Slf4j
@Service
public class JwtTokenServiceImpl implements JwtTokenService {

  private final RSAPrivateKey privateKey;
  private final RSAPublicKey publicKey;

  public JwtTokenServiceImpl(RSAPrivateKey privateKey,
                             RSAPublicKey publicKey) {
    this.privateKey = privateKey;
    this.publicKey = publicKey;
  }

  /**
   * @see JwtTokenService#createJwtAccessToken(UserDetails)
   * @param userDetails          User
   */
  @Override
  public String createJwtAccessToken(UserDetails userDetails) {

    // Create expiration date
    Calendar expiresAt = Calendar.getInstance();
    expiresAt.add(Calendar.MINUTE, SecurityConstants.ACCESS_TOKEN_DURATION_IN_MINUTES);

    // Create builder
    JWTCreator.Builder jwtBuilder = JWT.create().withSubject(userDetails.getUsername());

    // Add claims
    Map<String, String> claims = createClaims((UserPrincipal) userDetails);
    claims.forEach(jwtBuilder::withClaim);

    // Add expiredAt and etc
    return jwtBuilder
            .withNotBefore(Calendar.getInstance().getTime())
            .withExpiresAt(expiresAt.getTime())
            .sign(Algorithm.RSA256(publicKey, privateKey));
  }

  /**
   * @see JwtTokenService#createJwtRefreshToken(UserDetails)
   */
  @Override
  public String createJwtRefreshToken(UserDetails userDetails) {

    // Create expiration date
    Calendar expiresAt = Calendar.getInstance();
    expiresAt.add(Calendar.MINUTE, SecurityConstants.REFRESH_TOKEN_DURATION_IN_MINUTES);

    // Create builder
    JWTCreator.Builder jwtBuilder = JWT.create().withSubject(userDetails.getUsername());

    // Add claims
    Map<String, String> claims = createClaims((UserPrincipal) userDetails);
    claims.forEach(jwtBuilder::withClaim);

    // Add expiredAt and etc
    return jwtBuilder
            .withNotBefore(Calendar.getInstance().getTime())
            .withExpiresAt(expiresAt.getTime())
            .sign(Algorithm.RSA256(publicKey, privateKey));
  }

  /**
   * @see JwtTokenService#decodeToken(String)
   */
  @Override
  public Map<String, String> decodeToken(String token) {
    // Remove "Bearer " from token
    String refreshToken = StringUtils.removeStart(token, "Bearer").trim();

    DecodedJWT jwt;
    Map<String, String> result = new HashMap<>();
    try {
      // Verify token
      Algorithm algorithm = Algorithm.RSA256(publicKey, privateKey);
      JWTVerifier verifier = JWT.require(algorithm)
              .build(); //Reusable verifier instance
      jwt = verifier.verify(refreshToken);

      // Get claims
      Map<String, Claim> claims = jwt.getClaims();

      // Map claims
      claims.forEach((key, value) -> result.put(key, value.asString()));
    } catch (JWTVerificationException exception){
      //Invalid signature/claims
      log.error("Invalid token claims.");
      throw new UnauthorizedException();
    }
    // Return mapped claims
    return result;
  }

  /**
   * Create token claims
   * @param userPrincipal       User principal
   * @return                    Claims
   */
  private Map<String, String> createClaims(UserPrincipal userPrincipal) {
    Map<String, String> claims = new HashMap<>();

    // Add id
    claims.put("id", userPrincipal.getId().toString());

    // Add username
    claims.put("username", userPrincipal.getUsername());

    // Add user roles
    String authorities = userPrincipal.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.joining(" "));
    claims.put(SecurityConstants.AUTHORITIES_CLAIM_NAME, authorities);

    return claims;
  }

}
