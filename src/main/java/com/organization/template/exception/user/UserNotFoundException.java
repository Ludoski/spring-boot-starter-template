package com.organization.template.exception.user;

import com.organization.template.exception.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * User not found exception
 */
public class UserNotFoundException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "User does not exist.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.NOT_FOUND;

  public UserNotFoundException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
